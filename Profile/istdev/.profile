

#PATH=/usr/bin:/etc:/usr/sbin:/usr/ucb:$HOME/bin:/usr/bin/X11:/sbin:.
PATH=/usr/bin:/etc:/usr/sbin:/usr/ucb:$HOME/bin:/usr/bin/X11:/sbin:~jnarawu/mycode:.

export PATH

if [ -s "$MAIL" ]           # This is at Shell startup.  In normal
then echo "$MAILMSG"        # operation, the Shell checks
fi                          # periodically.

# Timeout is set to 600 seconds or 10 minutes
TMOUT=600
TIMEOUT=600
export readonly TMOUT TIMEOUT
PS1=`hostname`:$USER:'$PWD> '

stty erase ^?
set -o vi
export PS1=`whoami`@`uname -n`':$PWD $ '
export EDITOR=/usr/bin/vi

# The following three lines have been added by UDB DB2.
#if [ -f /db2home/db2ist/sqllib/db2profile ]; then
#    . /db2home/db2ist/sqllib/db2profile
#fi

# The following three lines have been added by UDB DB2.
if [ -f /home/db2clnt/sqllib/db2profile ]; then
    . /home/db2clnt/sqllib/db2profile
fi

if [ -f /opt/efunds/ist/ist_profile.ksh ]; then
        . /opt/efunds/ist/ist_profile.ksh
fi


alias l='ls -l'
alias extract='cd /opt/efunds/ist/product/log/extractrpt'
alias itmxrpt='cd /opt/efunds/ist/saved'

. $HOME/profiles/profile.alias

NODENAME=`uname -n`
export PS1="\$PWD
`tput smso`$LOGNAME@$NODENAME-HUB1_PRO>`tput rmso` "

banner "HUB1 PRO"
