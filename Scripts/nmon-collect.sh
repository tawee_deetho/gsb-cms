# Description
#
# This script is to collect statistics from all servers by using nmon
# The script will run every 5 minutes and then save data to local file.
# Put script in this path
# /root/scripts/nmon-collect.sh
# Create required path data/nmon,log under /opt/nmon
# ---------

#---- kim

WORK_DIR=/opt/nmon
DATA_DIR=$WORK_DIR/data/nmon
LOG_DIR="${WORK_DIR}/log"
export DATA_DIR WORK_DIR LOG_DIR
umask 0022

# Make sure data and log direcotry exists, otherwise create one
[ -d "$WORK_DIR" ] || mkdir -m 755 $WORK_DIR
[ -d "$DATA_DIR" ] || mkdir -m 755 -p $DATA_DIR
[ -d "$LOG_DIR" ] || mkdir -m 755 $LOG_DIR

# Create a daily NMON file - sampling once every 5 minute for 24 hours
#
nmon -fdT -s300 -c288 -m${DATA_DIR}

#
# Compress NMON files older than 7 days
#
find $DATA_DIR -xdev -name "*.nmon" -mtime +7 -type f -print |xargs -L 1 -I {} -t gzip --best {}

#
# Delete NMON files older than 181 days
#
find $DATA_DIR -xdev -name "*.nmon.gz" -mtime +181 -type f |xargs -L 1 -I {} -t rm {}


#add to crontab by crontab -e

# Run NMON to collect system stat   
#1 0 * * * /root/scripts/nmon-collect.sh > /opt/nmon/log/nmon-collect.log 2>&1
